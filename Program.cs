﻿using System;

namespace EmplProfile
{
    class Program
    {
        static void Main()
        {
            Employee emp1 = new Employee(
                "Artur Konst",
                EmployeePosition.Engineer,
                "Managment#3"
                );
            Employee emp2 = new Employee(
                "Ivanov Ivan",
                EmployeePosition.Versatile,
                "SHED#6"
                );
            Console.WriteLine(emp1);
            Console.WriteLine(emp2);
            emp1.setDepartmentName("IT");
            emp1.setEmployeePosition(EmployeePosition.Director);
            Console.WriteLine(emp1);

            Console.WriteLine("Employee 1 first time was Engineer in "
							+emp1.findFirstInEmployeeHistory("Engineer")+" line of work history");
            Console.WriteLine("Employee 1 first time was Director in "
							+emp1.findFirstInEmployeeHistory("Director")+" line of work history");

            Console.WriteLine((emp1 > emp2)? "Emp1 has higher position":"Emp2 has higher position");

            Console.WriteLine(emp1.payroll());

        }
    }
}
