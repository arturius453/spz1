﻿using System;
using System.Collections.Generic;

namespace EmplProfile
{
    class Employee :IComparable
    {
        private string _fullName;
        private EmployeePosition _employeePosition;
        private int _salary;
        private List<string> _employeeHistory;
        private string _departmentName;

        public void setDepartmentName(string department)
        {
            if(_departmentName != department)
            {
                departmentName = department;
                _employeeHistory.Add(_departmentName);
            }
        }
        public void setEmployeePosition(EmployeePosition position)
        {
            if(_employeePosition != position)
            {
                _employeePosition = position;
                salary = 0;
                _employeeHistory.Add(_employeePosition.ToString());
            }
        }
        public int findFirstInEmployeeHistory(string tofind)
        {
            for(int itemIndex = 0;itemIndex < _employeeHistory.Count; itemIndex++)
            {
                if(_employeeHistory[itemIndex] == tofind)
                {
                    return itemIndex;
                }
            }
            return -1;
        }
        public string payroll()
        {
				return $"Employee {_fullName} from the department {_departmentName} recived payment {_salary}";
        }
        public override string ToString()
        {
            return $"Employee {_fullName} from the department {_departmentName} in the positions of {_employeePosition.ToString()} with a salary of {_salary}";
        }

		public int CompareTo(object obj) {
	        if (obj == null) return 1;
	
	        Employee otherEmployee = obj as Employee;
	        if (otherEmployee != null)
	            return this._employeePosition.CompareTo(otherEmployee._employeePosition);
	        else
	           throw new ArgumentException("Object is not a Employee");
	    }
		public static bool operator >  (Employee operand1, Employee operand2) 
					=> operand1.CompareTo(operand2) > 0;
   		public static bool operator <  (Employee operand1, Employee operand2)
   		  			=> operand1.CompareTo(operand2) < 0;

        //Constructors
        public Employee(string fullName,
            EmployeePosition employeePosition,
            string departmentName)
        {
            this.fullName = fullName;
            this._employeePosition = employeePosition;
            this.departmentName = departmentName;
            salary=0;

            _employeeHistory = new List<string>();
            _employeeHistory.Add(departmentName);
            _employeeHistory.Add(employeePosition.ToString());
        }

        private int salary
        {
            get => _salary;
            set {
				switch(_employeePosition){
					case EmployeePosition.Versatile:_salary= 5000;break;
					case EmployeePosition.Engineer: _salary= 10000;break;
					case EmployeePosition.Manager: _salary= 20000;break;
					case EmployeePosition.Director: _salary= 50000;break;
				}
        	}
		}
        public string fullName
        {
            get => _fullName;
            set
            {
                if (!NameValidator.isValidFullName(value))
                {
                    throw new FormatException();
                }

                _fullName = value;
            }
        }
        private string departmentName
        {
            get => _departmentName;
            set
            {
                if (!NameValidator.isValidDepartmentName(value)) 
                {
                    throw new FormatException();
                }
                _departmentName = value;
            }
        }

    }
}
