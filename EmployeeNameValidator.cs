﻿using  System;
namespace EmplProfile
{
    class NameValidator
    {
        static public bool isValidFullName(string name)
        { 
			if (System.String.IsNullOrEmpty(name))
            	return false;
			foreach(char i in name)
					if(i>='0' && i<='9') return false;
            return true;

        }
        static public bool isValidDepartmentName(string name)
        {
			if (System.String.IsNullOrEmpty(name))
            	return false;
			bool hasLetter=false,hasNumber=false;
			foreach(char i in name)
					if(Char.IsLetter(i)) {hasNumber=true;break;}
			
			foreach(char i in name)
					if(Char.IsLetter(i))
				   	{hasLetter=true;break;}

            return hasLetter&&hasNumber;

        }

    }
}
